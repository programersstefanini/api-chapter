﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Domain.Data
{
   public interface IUnitOfWork : IDisposable
    {
        Task<bool> CommitAsync();

        DbContext GetContext();

        IRepositoryConsult<T> GetRepository<T>() where T : class;
    }
}
