﻿using Chapter.Crosscutting;
using Chapter.Domain.Customer;
using Chapter.Domain.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Domain.Models
{
   public interface IBaseService<TEntity> : IDisposable where TEntity : EntityDataBase
    {
        public IBaseRepository<TEntity> _iBaseRepository { get; }


        public LNotifications LNotifications { get; }


        void Add(TEntity entidade);

        void Update(TEntity customer);

        void Remove(TEntity customer);

        void Remove<T>(T customer) where T : class;

        Task AddAsync(TEntity entidade);

       

        Task AddAsync<T>(T entidade) where T : EntityDataBase;


    }
}
