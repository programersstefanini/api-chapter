﻿using Chapter.Crosscutting;
using Chapter.Domain.Customer;
using Chapter.Domain.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Domain.Models
{
   public abstract class BaseServices<TEntity> : 
                          IBaseService<TEntity> where TEntity : EntityDataBase
    {

        public void Dispose() => GC.SuppressFinalize(this);

        public void Add(TEntity entidade)
        {
            _iBaseRepository.Add(entidade);
        }

        public void Update(TEntity customer)
        {
            _iBaseRepository.Update(customer);
        }

        public void Remove(TEntity customer)
        {
            _iBaseRepository.Remove(customer);
        }

        public void Remove<T>(T customer) where T : class
        {
            _iBaseRepository.Remove(customer);
        }

        public async Task AddAsync(TEntity entidade)
        {
            await _iBaseRepository.AddAsync(entidade);
        }

    

        public async Task AddAsync<T>(T entidade) where T : EntityDataBase
        {
            await _iBaseRepository.AddAsync(entidade);
        }

        public IBaseRepository<TEntity> _iBaseRepository { get; protected set; }

        public LNotifications LNotifications { get; protected set; }

        public BaseServices(
              IBaseRepository<TEntity> iBaseRepository,
              LNotifications lNotifications
            )
        {

            LNotifications = lNotifications;
            _iBaseRepository = iBaseRepository;

        }

    }
}
