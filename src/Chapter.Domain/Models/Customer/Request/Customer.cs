﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Domain.Models.Customer.Request
{
    public abstract class Customer
    {

        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }

    }
}
