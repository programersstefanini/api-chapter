﻿using Chapter.Crosscutting;
using Chapter.Domain.Customer;
using Chapter.Domain.Models.Customer.Request;
using Chapter.Domain.Models.Customer.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Domain.Models.Customer.Interfaces
{
    public interface ICustomerServices: IBaseService<CustomerDomain>
    {

        Task<CustomerInsertResponse> Save(CustomerInsertRequest customerInsertRequest);

        Task<PagedDataResponse<CustomerGetResponse>> Get(CustomerGetRequest customerGetRequest);


    }
}
