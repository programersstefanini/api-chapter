﻿using AutoMapper;
using Chapter.Crosscutting;
using Chapter.Domain.Customer;
using Chapter.Domain.Data;
using Chapter.Domain.Models.Customer.Interfaces;
using Chapter.Domain.Models.Customer.Request;
using Chapter.Domain.Models.Customer.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Domain.Models.Customer
{
    public class CustomerServices : BaseServices<CustomerDomain>, ICustomerServices
    {

        readonly IMapper _mapper;
        public CustomerServices(ICustomerRepository customerRepository,
                                IMapper mapper,
            LNotifications lNotifications) : base(customerRepository, lNotifications)
        {

            _mapper = mapper;
        }

        public async Task<PagedDataResponse<CustomerGetResponse>> Get(CustomerGetRequest customerGetRequest)
        {

            var pagedDataResponse = new PagedDataResponse<CustomerGetResponse>();
            var query = _iBaseRepository._repositoryConsult.GetQueryable();

            if (!string.IsNullOrEmpty(customerGetRequest.Nome))
                query = query.Where(x => x.Nome.Contains(customerGetRequest.Nome));

            var responseQuery = await query.PaginateAsync(customerGetRequest);
            pagedDataResponse.Page = responseQuery.Page;
            pagedDataResponse.PageSize = responseQuery.PageSize;
            pagedDataResponse.TotalItens = responseQuery.TotalItens;
            pagedDataResponse.TotalPages = responseQuery.TotalPages;

            pagedDataResponse.Items = responseQuery.Items.Select(x => new CustomerGetResponse
            {
                CPF = x.CPF.FormatCPF(),
                Endereco = x.Endereco,
                Nome = x.Nome

            }).ToList();


            return pagedDataResponse;

        }

        public async Task<CustomerInsertResponse> Save(CustomerInsertRequest customerInsertRequest)
        {
            var customerSave = _mapper.Map<CustomerDomain>(customerInsertRequest);
            await AddAsync(customerSave);
            return _mapper.Map<CustomerInsertResponse>(customerSave);
        }
    }
}
