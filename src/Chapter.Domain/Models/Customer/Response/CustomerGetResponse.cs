﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Domain.Models.Customer.Response
{
   public class CustomerGetResponse
    {
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
    }
}
