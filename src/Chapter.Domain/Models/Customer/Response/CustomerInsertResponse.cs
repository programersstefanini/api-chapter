﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Domain.Models.Customer.Response
{
   public class CustomerInsertResponse
    {
        public Guid Id { get; set; }
    }
}
