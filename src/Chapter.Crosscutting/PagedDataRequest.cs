﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Crosscutting
{
   public abstract class PagedDataRequest
    {
        public int Page { get; set; }

        public int Limit { get; set; }

        protected PagedDataRequest()
        {

            Limit = 30;

            Page = 1;

        }


    }
}
