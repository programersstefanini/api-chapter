﻿using Chapter.Application.Interfaces;
using Chapter.Application.Services;
using Chapter.Data.Context;
using Chapter.Data.Repository;
using Chapter.Data.Uow;
using Chapter.Domain.Data;
using Chapter.Domain.Models.Customer;
using Chapter.Domain.Models.Customer.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Crosscutting.Ioc
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services,
                                                        IConfiguration configuration,
                                                        IHostEnvironment hostEnvironment
                                                        )
        {

            
            services.AddScoped<LNotifications>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ICustomerApp, CustomerApp>();

            services.AddScoped<AplicationContext>();

            services.AddScoped<ICustomerServices, CustomerServices>();

            services.AddScoped<ICustomerRepository, CustomerRepository>();


        }
    }
}
