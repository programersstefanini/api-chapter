﻿using Chapter.Data.Context;
using Chapter.Data.Repository;
using Chapter.Domain.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Data.Uow
{
   public class UnitOfWork : IUnitOfWork
    {

        readonly AplicationContext _aplicationContext;

        public UnitOfWork(AplicationContext aplicationContext)
        {
            _aplicationContext = aplicationContext; 
        }

        public async Task<bool> CommitAsync() => await _aplicationContext.SaveChangesAsync() > 0;

        public void Dispose() => GC.SuppressFinalize(this);

        public DbContext GetContext() => _aplicationContext;

        public IRepositoryConsult<T> GetRepository<T>() where T : class => new RepositoryConsult<T>(GetContext());

    }
}
