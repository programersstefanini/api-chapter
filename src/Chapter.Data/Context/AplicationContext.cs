﻿using Chapter.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Data.Context
{
  public  class AplicationContext : DbContext
    {


        public AplicationContext(DbContextOptions<AplicationContext> options)
         : base(options)
        {




        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerMapping());
        }

    }
}
