﻿using Chapter.Domain.Customer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Data.Mapping
{
   public class CustomerMapping: BaseMapping<CustomerDomain>
    {


        public override void Configure(EntityTypeBuilder<CustomerDomain> builder)
        {
            base.Configure(builder);


            builder.Property(x => x.CPF)
                .HasColumnName("CPF")
                .HasColumnType("varchar(20)")
                
                ;

            builder.Property(x => x.Nome)
                 .HasColumnName("Nome")
                 .HasColumnType("varchar(200)")

                   ;

            builder.Property(x => x.Endereco)
               .HasColumnName("Endereco")
               .HasColumnType("varchar(200)")

                 ;
            builder.ToTable("Customer");

        }



    }
}
