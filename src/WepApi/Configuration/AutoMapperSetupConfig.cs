﻿using Chapter.Application.Automapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WepApi.Configuration
{
    public static class AutoMapperSetupConfig
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(DomainToResponseModelMappingProfile),
                                   typeof(RequestToDomainMappingProfile)
                                   );
        }
    }
}
