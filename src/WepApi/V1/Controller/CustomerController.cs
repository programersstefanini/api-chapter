﻿using Chapter.Application.Interfaces;
using Chapter.Crosscutting;
using Chapter.Domain.Models.Customer.Request;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WepApi.Controllers;

namespace WepApi.V1.Controller
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/customer")]

    public class CustomerController : MainController
    {

        readonly ICustomerApp _customerApp;
        public CustomerController(ICustomerApp customerApp, LNotifications notifications) : base(notifications)
        {

            _customerApp = customerApp;
        }


        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] CustomerGetRequest customerGetRequest)
            => await ExecControllerAsync(() => _customerApp.Get(customerGetRequest));

        

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CustomerInsertRequest customerInsertRequest)
            => await ExecControllerAsync(() => _customerApp.Save(customerInsertRequest));


        [HttpPut]
        public async Task<IActionResult> Put() => Ok(await Task.FromResult(true));

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete() => Ok(await Task.FromResult(true));


    }
}
