﻿using Chapter.Application.Interfaces;
using Chapter.Crosscutting;
using Chapter.Domain.Data;
using Chapter.Domain.Models.Customer.Interfaces;
using Chapter.Domain.Models.Customer.Request;
using Chapter.Domain.Models.Customer.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Application.Services
{
    public class CustomerApp : BaseApp, ICustomerApp
    {
        readonly ICustomerServices _customerServices;
        public CustomerApp(IUnitOfWork _unitOfWork,
                           LNotifications lNotifications,
                           ICustomerServices customerServices) 
            : base(  _unitOfWork: _unitOfWork,
                  lNotifications: lNotifications)
        {
            _customerServices = customerServices;
        }

        public async Task<PagedDataResponse<CustomerGetResponse>> Get(CustomerGetRequest customerGetRequest)
        => await _customerServices.Get(customerGetRequest);

        public async Task<CustomerInsertResponse> 
            Save(CustomerInsertRequest custumerInserRequest)
        {
            var ret = await _customerServices.Save(custumerInserRequest);
            await unitOfWork.CommitAsync();
            return ret;
        }
    }
}
