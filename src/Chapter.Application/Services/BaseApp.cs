﻿using Chapter.Application.Interfaces;
using Chapter.Crosscutting;
using Chapter.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapter.Application.Services
{
    public abstract class BaseApp : IBaseApp
    {
       protected  readonly LNotifications LNotifications;

        public BaseApp(IUnitOfWork _unitOfWork, LNotifications lNotifications)
        {
            if (lNotifications == null)
                lNotifications = new LNotifications();

            LNotifications = lNotifications;
            unitOfWork = _unitOfWork;
        }

        public IUnitOfWork unitOfWork { get; protected set; }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
