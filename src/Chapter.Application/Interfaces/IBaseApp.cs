﻿using Chapter.Crosscutting;
using Chapter.Domain.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chapter.Application.Interfaces
{
    public interface IBaseApp : IDisposable
    {

        public IUnitOfWork unitOfWork { get; }

    }
}
