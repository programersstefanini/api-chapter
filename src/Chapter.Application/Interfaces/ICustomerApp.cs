﻿using Chapter.Crosscutting;
using Chapter.Domain.Models.Customer.Request;
using Chapter.Domain.Models.Customer.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chapter.Application.Interfaces
{
   public interface ICustomerApp
    {

        Task<CustomerInsertResponse> Save(CustomerInsertRequest custumerInserRequest);

        Task<PagedDataResponse<CustomerGetResponse>> Get(CustomerGetRequest customerGetRequest);

    }
}
